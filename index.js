const path = require("path");
const fs = require("fs");
const express = require("express");
const morgan = require("morgan");
const { promisify } = require("util");

const readFileAsync = promisify(fs.readFile);
const writeFileAsync = promisify(fs.writeFile);
const accessAsync = promisify(fs.access);
const mkdirAsync = promisify(fs.mkdir);
const readdirAsync = promisify(fs.readdir);

const app = express();
const port = 8080;
app.use(express.json());
app.use(morgan("tiny"));

app.post("/api/files", async (req, res, next) => {
  if (req.body.content === undefined) {
    res.status(400).json({
      message: "Please specify 'content' parameter",
    });
    return;
  }

  if (req.body.filename === undefined) {
    res.status(400).json({
      message: "Please specify 'filename' parameter",
    });
    return;
  }

  const validExtensions = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];
  const isValid = validExtensions.some((ext) =>
    req.body.filename.endsWith(ext)
  );

  if (!isValid) {
    res.status(400).json({
      message: "Please specify 'filename' parameter",
    });
    return;
  }
  const dir = "./api";

  try {
    await accessAsync(dir, fs.constants.F_OK);
  } catch (error) {
    await mkdirAsync(path.join(__dirname, dir));
  }

  try {
    await writeFileAsync(
      path.join(__dirname, dir, req.body.filename),
      req.body.content
    );
    res.status(200).json({
      message: "File created successfully",
    });
  } catch (error) {
    next(error);
  }
});

app.put("/api/files", async (req, res, next) => {
  if (req.body.content === undefined) {
    res.status(400).json({
      message: "Please specify 'content' parameter",
    });
    return;
  }

  if (req.body.filename === undefined) {
    res.status(400).json({
      message: "Please specify 'filename' parameter",
    });
    return;
  }

  try {
    const file = await readFileAsync(
      path.join(__dirname, dir, req.body.filename, { encoding: "utf8" })
    );

    await writeFileAsyncpath.join(
      __dirname,
      dir,
      req.body.filename,
      req.body.content
    );
  } catch (err) {
    next(err);
  }
});

app.get("/api/files", async (req, res) => {
  try {
    const files = await readdirAsync("./api");
    res.json({
      message: "success",
      files,
    });
  } catch (err) {
    res.json({
      message: "success",
      files: [],
    });
  }
});

app.get("/api/files/:filename", async (req, res, next) => {
  if (req.params.filename === undefined) {
    res.status(400).json({
      message: "Please specify 'filename' parameter",
    });
    return;
  }
  const dir = "./api";
  try {
    const fileContents = await readFileAsync(
      path.join(__dirname, dir, req.params.filename),
      { encoding: "utf8" }
    );
    const splitted = req.params.filename.split(".");
    const ext = splitted[splitted.length - 1];
    const stats = fs.statSync(path.join(__dirname, dir, req.params.filename));
    const uploadDate = stats.birthtime;
    res.status(200).json({
      message: "success",
      filename: req.params.filename,
      content: fileContents,
      extension: ext,
      uploadedDate: uploadDate,
    });
  } catch (err) {
    res.status(400).json({
      message: `No file with '${req.params.filename}' filename found`,
    });
  }
});

app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).json({
    message: "Server error",
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
